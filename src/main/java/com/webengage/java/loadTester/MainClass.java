package com.webengage.java.loadTester;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.URL;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

public class MainClass {
    long LOWER_RANGE = 0;
    long UPPER_RANGE = 9999999999L;

    private static MainClass mainClass;
    private AtomicLong al = new AtomicLong(0);
    private long startTimestamp;
    private Long oldRequestCount = 0L;
    private int threadsToSpwan = 100;

    private Random random = new Random();
    private Long randomStartValue = LOWER_RANGE + (long)(random.nextDouble()*(UPPER_RANGE - LOWER_RANGE));

    public static void main(String[] args){
        mainClass = new MainClass();

        if(args.length > 0) {
            try {
                mainClass.threadsToSpwan = Integer.parseInt(args[0]);
                System.out.println("Spawning " + mainClass.threadsToSpwan + " threads...");
            } catch(Exception e){
                e.printStackTrace();
                System.exit(1);
            }
        }
        mainClass.loadTest();
    }

    private void loadTest() {
        startTimestamp = new Date().getTime();

        new RequestRateCounter().start();

        for(int i = 0; i < mainClass.threadsToSpwan; i++){
            new LoadRequest("thread - " + i).start();
        }
    }

    class LoadRequest extends Thread {
        private String threadName;

        public LoadRequest(String threadName){
            this.threadName = threadName;
        }

        @Override
        public void run() {
            long numRequestsToMake = 1000;
            Long requestCount = 0L;
            BufferedReader rd;
            OutputStreamWriter wr;
            long errorCount = 0;

            while(requestCount <= numRequestsToMake) {
                try {
                    URL obj = new URL("http://ec2-54-184-125-56.us-west-2.compute.amazonaws.com/coupon/~15ba20999/luid_" + randomStartValue.toString());
                    randomStartValue++;
                    requestCount++;
                    al.incrementAndGet();

                    HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                    con.setRequestMethod("GET");

                    try {
                        int responseCode = con.getResponseCode();
                        if(responseCode != 200) {
                            errorCount++;
                        } else {
                            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                            String responseText = in.readLine();
                            if(responseText.indexOf("\"status\":\"success\"") < 0) {
                                errorCount++;
                            }
                        }
                    } catch(SocketException e){
                        errorCount++;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //System.out.println(threadName + ": " + errorCount + "/" + requestCount);
            }
        }
    }

    class RequestRateCounter extends Thread {
        @Override
        public void run() {
            while(true) {
                long currentRequestCount = al.get();
                long rate = currentRequestCount  - oldRequestCount;
                System.out.println("Request rate: " + rate);
                oldRequestCount = currentRequestCount ;

                sendRate(rate);

                try {
                    Thread.currentThread().sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void sendRate(Long combinedRequestRate){
        try {
            URL requestRateSendUrl = new URL("http://localhost:4444/requestRate?rate=" + combinedRequestRate.toString());
            try {
                HttpURLConnection con = (HttpURLConnection) requestRateSendUrl.openConnection();
                con.setRequestMethod("GET");
                try {
                    try {
                        int responseCode = con.getResponseCode();
                        if(responseCode == 200) {
                            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                            String responseText = in.readLine();
                            if(responseText.indexOf("stop") > -1) {
                                System.exit(1);
                            }
                        }
                    } catch(SocketException e){

                    }
                } catch(SocketException e){

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}

#!/bin/sh

if [ "$#" -ne 2 ]; then
  echo "Usage: $0 <number_of_threads_per_process> <number_of_parallel_processes>" >&2
  exit 1
fi

for i in {1..$2}
do 
    java -jar target/load-tester-1.0-SNAPSHOT.one-jar.jar 10 "$1" > ./llog.${i}.log &
done

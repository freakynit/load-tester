var express = require('express');
var router = express.Router();
var exec = require('child_process').exec;

var timerPeriodInSeconds = 3;
var combinedRequestRate = 0;
var itvl = null;
var sendStopSignal = false;

function countRequestRate(){
	console.log("combinedRequestRate = " + (combinedRequestRate/timerPeriodInSeconds));
	combinedRequestRate = 0;
}

router.get('/sendStopSignal', function(req, res) {
	sendStopSignal = true;
	res.json({"success": true});
});

router.get('/requestRate', function(req, res) {
	if(itvl == null) {
		itvl = setInterval(countRequestRate, timerPeriodInSeconds * 1000);
	}

	var requestRate = req.query.rate;
	if(!requestRate) {
		requestRate = 0;
	}

	combinedRequestRate += parseFloat(requestRate);
	
	if(sendStopSignal == false) {
		res.json({"success": true, "requestRate": (combinedRequestRate/timerPeriodInSeconds)});
	} else {
		res.send("stop")
	}
});

module.exports = router;

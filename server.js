var PORT = 4444;

var express = require('express');

var app = express();
var index = require('./routes/index');
app.use(index);

app.listen(PORT, function(){
  console.log("Started on PORT " + PORT);
});
